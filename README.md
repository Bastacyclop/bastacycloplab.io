# Thomas Koehler's website

My personal static website, generated using [Scala](https://scala-lang.org).

## How to generate

```sh
./generate // requires sh, sbt
```

## Local testing

```sh
./preview // requires sh, python, exo-open
```

## TODO

- Research page?
- Service page
- Teaching page?
- Students page?

- paragraph start/break issues `<em/><p></p>` instead of `<p><em/></p>` in about.me
- Sitemap
- RSS Feed
- More sections? Projects/Blog/..
- Site icon ?
- Multiple languages/attributes ?
- Download Prism.js during build ?

## Certbot

```sh
certbot certonly -a manual -d thok.eu # Challenge
cat /etc/letsencrypt/live/thok.eu/fullchain.pem # Certificate (PEM)
cat /etc/letsencrypt/live/thok.eu/privkey.pem # Key (PEM)
```
