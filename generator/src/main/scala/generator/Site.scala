package generator

object Site extends App {
  val author = "Thomas K&oelig;hler"
  val root = Path.fromString("..")

  Style.genSheet()
  genRoot()
  genTalks()
  genPublications()
  genSupervisions()
  // genPosters()

  var posts = new collection.mutable.ArrayBuffer[Post]()
  File.forAllFiles(root / "blog",
    path => if (path.toString().endsWith(".me")) { Post.process(path, posts) })

  genBlogRoot(posts.toList)

  def genRoot() = {
    val path = root / "public" / "index.html"
    val about = Markup.parse(root / "about.me").get.html

    Style.genPage(
      path,
      s"<title>$author</title>",
      "",
      s"""
$about

<h2>Contact <a href="mailto:thomas.koehler@cnrs.fr"><i class="fa fa-envelope"></i></a></h2>
      """
    )
  }

  def genBlogRoot(posts: List[Post]) = {
    val path = root / "public" / "blog" / "index.html"

    Style.genPage(
      path,
      s"<title>$author's blog</title>",
      "<h1>Blog</h1>",
      s"""
<ul>
  ${
        posts
          .sortBy({ _.date })(Ordering[String].reverse)
          .map({ p =>
            val path = root.relativize(p.path).withExtension("html")
            s"""<li><a href="/$path">${p.title}</a> ${p.date} ${p.tags.mkString(" ")}</li>"""
          })
          .mkString("\n")
      }
</ul>
"""
    )
  }

  def linkIfExists(path: Path, content: String): String = {
    val relSite = root.relativize(path)
    val relDisk = Path.fromString("../public/") / relSite
    if (relDisk.exists()) {
      s"""<a href="/${relSite}">${content}</a>"""
    } else {
      println(s"missing $relDisk")
      content
    }
  }

  def prettyDate(date: String): String = {
    val parts = date.split('-')
    val year = parts(0)
    val month = parts(1)
    val sMonth = month match {
      case "01" => "January"
      case "02" => "February"
      case "03" => "March"
      case "04" => "April"
      case "05" => "May"
      case "06" => "June"
      case "07" => "July"
      case "08" => "August"
      case "09" => "September"
      case "10" => "October"
      case "11" => "November"
      case "12" => "December"
    }
    s"${sMonth} ${year}"
  }

  def genTalks() = {
    val path = root / "public" / "talks" / "index.html"

    final case class Talk(path: Path, title: String, date: String, boldvenue: String, venue: String, description: String)

    var talks = new collection.mutable.ArrayBuffer[Talk]()
    File.forAllFiles(root / "talks",
      path => if (path.toString().endsWith(".me")) {
        def getOrFail[T](opt: Option[T], reason: String) =
          opt.getOrElse(throw new RuntimeException(s"${reason} @ ${path}"))
        val p = getOrFail(Markup.parse(path), "parsing failed")
        talks += Talk(path, getOrFail(p.title, "missing title"), getOrFail(p.date, "missing date"), p.boldvenue.map(s => s + " ").getOrElse(""), getOrFail(p.venue, "missing venue"), p.html.result)
      })

    Style.genPage(
      path,
      s"<title>$author's talks</title>",
      "<h1>Talks</h1>",
      s"""
<ul>
  ${
        talks
          .sortBy({ _.date })(Ordering[String].reverse)
          .map({ t =>
            val slides = t.path.withExtension("pdf")
            val title = linkIfExists(slides, s"""<strong>${t.title}</strong>""")
            s"""<li>${title}<br/><strong>${t.boldvenue}</strong><em>${t.venue}</em><br/>${t.date}${t.description}</li>"""
          })
          .mkString("\n")
      }
</ul>
"""
    )
  }

  def genPublications() = {
    val path = root / "public" / "publications" / "index.html"

    final case class Publication(path: Path, pdf: String, title: String, author: String, date: String, boldvenue: String, venue: String, description: String)

    var pubs = new collection.mutable.ArrayBuffer[Publication]()
    File.forAllFiles(root / "publications",
      path => if (path.toString().endsWith(".me")) {
        Markup.parse(path) match {
          case Some(p) => {
            val pdf = p.link.getOrElse(s"/${root.relativize(path).withExtension("pdf")}")
            pubs += Publication(path, pdf, p.title.get, p.author.get, p.date.get, p.boldvenue.map(s => s + " ").getOrElse(""), p.venue.get, p.html.result)
          }
          case _ =>
        }
      }
    )

    Style.genPage(
      path,
      s"<title>$author's publications</title>",
      "<h1>Publications</h1>",
      s"""
<ul>
  ${
        pubs
          .sortBy({ _.date })(Ordering[String].reverse)
          .map({ t =>
            s"""<li>${t.author}:<br/><strong><a href="${t.pdf}">${t.title}</a></strong><br/><strong>${t.boldvenue}</strong><em>${t.venue}</em><br/>${t.date}${t.description}</li>"""
          })
          .mkString("\n")
      }
</ul>
"""
    )
  }

  def genSupervisions() {
    val path = root / "public" / "supervisions" / "index.html"

    final case class Topic(path: Path, title: String, researcher: Option[String], date: String, description: String)

    var available_masters = new collection.mutable.ArrayBuffer[Topic]()
    var current_phds = new collection.mutable.ArrayBuffer[Topic]()
    var current_masters = new collection.mutable.ArrayBuffer[Topic]()

    File.forAllFiles(root / "supervisions",
      path => if (path.toString().endsWith(".me")) {
        Markup.parse(path) match {
          case Some(p) => {
            // val pdf = p.link.getOrElse(s"/${root.relativize(path).withExtension("pdf")}")
            val topic = Topic(path, p.title.get, p.researcher, p.date.get, p.html.result.drop(3).dropRight(4))
            (p.level.get, p.researcher) match {
              case ("Master", None) => available_masters += topic
              case ("Master", Some(_)) => current_masters += topic
              case ("PhD", None) => () // TODO
              case ("PhD", Some(_)) => current_phds += topic
            }
          }
          case _ => {}
        }
      })

    val available_master_topics = if (available_masters.isEmpty) { "" } else {
        s"""
<h2>Available Master Topics</h2>
<ul>
  ${
    available_masters
      .sortBy({ _.date })(Ordering[String].reverse)
      .map({ t =>
        val title = linkIfExists(t.path.withExtension("pdf"), t.title)
        s"""<li><strong>${title}</strong><br/>updated on ${t.date}${t.description}</li>"""
      })
      .mkString("\n")
  }
</ul>
    """
    }

    val current_phd_topics = if (current_phds.isEmpty) { "" } else {
      s"""
<h2>Current PhD Researchers</h2>
<ul>
  ${
    current_phds
      .sortBy({ _.date})
      .map({ t =>
        val title = t.title // linkIfExists(t.path.withExtension("pdf"), t.title)
        s"""<li>${t.researcher.get}: <strong>${title}</strong><br/>${t.description}<i> since ${prettyDate(t.date)}</i><p></p></li>"""
      })
      .mkString("\n")
  }
</ul>
      """
    }

    val current_master_topics = if (current_masters.isEmpty) { "" } else {
      s"""
<h2>Current Master Researchers</h2>
<ul>
  ${
    current_masters
      .sortBy({ _.date})
      .map({ t =>
        val title = t.title // linkIfExists(t.path.withExtension("pdf"), t.title)
        s"""<li>${t.researcher.get}: <strong>${title}</a></strong><br/>${t.description}<i> since ${prettyDate(t.date)}</i><p></p></li>"""
      })
      .mkString("\n")
  }
</ul>
      """
    }

    Style.genPage(path,
      s"<title>$author's supervisions</title>",
      "<h1>Supervisions and Topics</h1>",
      s"""
${available_master_topics}
${current_phd_topics}
${current_master_topics}
</ul>
      """)
  }

  // DEPRECATED: folded into Talks
  def genPosters() = {
    val path = root / "public" / "posters" / "index.html"

    final case class Poster(path: Path, title: String, date: String, venue: String, description: String)

    var posters = new collection.mutable.ArrayBuffer[Poster]()
    File.forAllFiles(root / "posters",
      path => if (path.toString().endsWith(".me")) {
        Markup.parse(path) match {
          case Some(p) => { posters += Poster(path, p.title.get, p.date.get, p.venue.get, p.html.result) }
          case _ => {}
        }
      })

    Style.genPage(
      path,
      s"<title>$author's posters</title>",
      "<h1>Posters</h1>",
      s"""
<ul>
  ${
        posters
          .sortBy({ _.date })(Ordering[String].reverse)
          .map({ t =>
            val slides = root.relativize(t.path).withExtension("pdf")
            s"""<li><a href="/$slides"><strong>${t.title}</strong></a><br/><em>${t.venue}</em><br/>${t.date}${t.description}</li>"""
          })
          .mkString("\n")
      }
</ul>
"""
    )
  }
}
