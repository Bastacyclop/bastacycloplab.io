package generator

object Style {
  val black = "#303030"
  val main_color = "#d6f285"
  val link_color = "#c95823"
  val main_font = "Lora"
  val title_font = "Raleway"
  val code_font = "Source Code Pro"
  val fonts = Array(main_font, title_font, code_font)

  val navicons = Array(
    ("Supervisions", true, "/supervisions", "fa fa-graduation-cap"),
    ("Publications", true, "/publications", "fa fa-pencil-alt"),
    ("Talks", true, "/talks", "fa fa-comment"),
    // ("Posters", true, "/posters", "fa fa-image"), // fa-paint-brush
    // Projects people-carry
    // ("Teaching", "/teaching", "fa fa-chalkboard-teacher"),
    // ("News", "/news", "fa fa-news"),
    ("CV", true, "/curriculum-vitae.pdf", "fa fa-address-card"),
    ("GitLab", false, "https://gitlab.com/Bastacyclop", "fab fa-gitlab"),
    ("GitHub", false, "https://github.com/Bastacyclop", "fab fa-github"),
    // ("/atom.xml", "fa fa-rss"),
    // ("/contact.html", "fa fa-envelope")
  )

  def genSheet() = {
    val path = Site.root / "public" / "style.css"
    val contents = s"""
html, body {
  font-family: $main_font, serif;
  color: $black;
  font-size: 18px;
  box-sizing: border-box;
  margin: 0;
}
body {
  min-height: 100vh;
  display: flex;
  flex-direction: column;
}
h1, h2, h3, h4, h5, h6 {
  font-family: $title_font, sans-serif;
  font-weight: bold;
}
#header {
  background-color: $main_color;
  color: $black;
  padding: 0.5rem;
  overflow: hidden;
}
#header #home {
  font-weight: bold;
}
#header a {
  color: inherit;
  text-decoration: none;
  display: inline-block;
  margin: 0.1rem 0.5rem;
}
#header a:hover {
  color: $link_color;
}
#header h1 {
  margin: 0.1rem 0.5rem;
  font-size: 1em;
  font-style: italic;
  font-weight: normal;
  display: inline-block;
}
#header time {
  margin: 0.1rem 0.5rem;
  font-size: 0.8rem;
  font-weight: light;
  display: inline-block;
}
#header nav {
  display: inline-block;
  float: right;
}
#footer {
  background-color: $main_color;
  font-size: 0.8rem;
  padding: 1ex;
  margin-top: auto;
}
main {
  margin: 2em 5%;
}
main p {
  max-width: 50em;
  text-align: justify;
}
/*#main-text p {
  margin-left: auto;
  margin-right: auto;
}*/
a {
  color: $link_color;
  text-decoration: none;
}
a:hover {
  text-decoration: underline;
}
code[class*="language-"] {
  font-family: $code_font, monospace;
}
    """

    File.generate(path, contents)
  }
  
  def genPage(path: Path, head_ext: String, title: String, body: String) = {
    val contents = s"""
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="author" content="${Site.author}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=${fonts.mkString("|").replace(' ', '+')}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href="/style.css">
$head_ext
</head>

<body>

<header id="header">
<a id="home" href=/>${Site.author}</a>
$title
<nav>
  ${navicons.map({ case (title, showTitle, path, icon) => s"""<a title="$title" href="$path"><i class="$icon"></i>${if (showTitle) { " " + title } else { "" }}</a>""" }).mkString("\n  ")}
</nav>
</header>

<main${if (title == "") { " id=\"main-text\"" } else ""}>
$body
</main>

<footer id="footer">
<i class="far fa-copyright"></i> ${java.time.Year.now.getValue} ${Site.author}.
Generator written in <a href="https://www.scala-lang.org/">Scala</a>
while reading the <a href="https://developer.mozilla.org">MDN web docs</a>.
Powered by <a href="https://fonts.google.com/">google fonts</a>,
<a href="https://fontawesome.com/">fontawesome</a> icons,
and the <a href="https://prismjs.com/">Prism</a> syntax highlighter.
</footer>

</body>

</html>
    """

    File.generate(path, contents)
  }
}
