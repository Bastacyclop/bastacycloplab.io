package generator

import java.nio.file
import file.Files
import java.nio.charset.StandardCharsets

class Path(val inner: file.Path) {
  override def toString: String = inner.toString

  def withExtension(ext: String): Path = {
    val s = toString
    Path.fromString(s.substring(0, s.lastIndexOf('.') + 1) + ext)
  }

  def /(other: String): Path = {
    new Path(inner.resolve(other))
  }
  
  def /(other: Path): Path = {
    new Path(inner.resolve(other.inner))
  }

  def relativize(other: Path): Path = {
    new Path(inner.relativize(other.inner))
  }

  def exists(): Boolean = {
    Files.exists(inner)
  }
}

object Path {
  def fromString(first: String, more: String*): Path = {
    new Path(file.Paths.get(first, more:_*))
  }
}

object File {
  private def _forAllFiles(p: file.Path, f: Path => Unit): Unit = {

    Files.list(p).forEach(child => {
      if (Files.isDirectory(child)) {
        _forAllFiles(child, f)
      } else if (Files.isRegularFile(child)) {
        f(new Path(child))
      }
    })
  }

  def forAllFiles(p: Path, f: Path => Unit) = _forAllFiles(p.inner, f)

  def toSource(p: Path): io.Source = {
    io.Source.fromFile(p.toString)
  }

  def generate(p: Path, s: String) = {
    println(s"> $p")
    Files.createDirectories(p.inner.getParent)
    Files.write(p.inner, s.getBytes(StandardCharsets.UTF_8))
  }
}
