package generator

import scala.annotation.tailrec

object Markup {
  def parse(input: String): Option[Parser] = {
    val p = new Parser(input)
    p.items.commit.scan(new Position(index = 0, line = 1, column = 0)) match {
      case s: Success[_] => {
        commentPosition(Info, "success", s.pos, input)
        Some(p)
      }
      case FatalError(e, p, infos) => {
        commentPosition(Error, e, p, input)
        infos.foreach({ case (e, p) => commentPosition(Info, e, p, input) })
        None
      }
    }
  }

  def parse(path: Path): Option[Parser] = {
    parse(File.toSource(path).mkString)
  }

  sealed abstract class CommentKind
  final case object Error extends CommentKind
  final case object Warning extends CommentKind
  final case object Info extends CommentKind

  final case class Position(index: Int, line: Int, column: Int)

  def commentPosition(kind: CommentKind, msg: String, p: Position, text: String) = {
    val (color, kindStr) = kind match {
      case Error => (Console.RED, "Error")
      case Warning => (Console.YELLOW, "Warning")
      case Info => (Console.BLUE, "Info")
    }
    println(s"$color$kindStr l.${p.line} c.${p.column}: $msg")
    val lineStart = text.lastIndexOf('\n', p.index - 1) match {
      case -1 => 0
      case i => i + 1
    }
    val lineEnd = text.indexOf('\n', p.index) match {
      case -1 => text.length
      case i => i
    }
    println(s"${Console.BLACK}${text.slice(lineStart, lineEnd)}")
    print(color)
    (0 until p.column).foreach({ _ => print('~') })
    println('^')
    print(Console.RESET)
  }

  sealed abstract class Result[+T] {
    def >>=[U](f: Success[T] => Result[U]): Result[U] =
      this match {
        case s: Success[_] => f(s)
        case f: Failure => f
        case e: FatalError => e
      }

    def >>![U >: T](f: Failure => Result[U]): Result[U] =
      this match {
        case e: Failure => f(e)
        case x => x
      }

    def map[U](f: Success[T] => Success[U]): Result[U] =
      this >>= f

    def commit: Result[T] =
      this >>! { case Failure(e, p, infos) => FatalError(e, p, infos) }

    def pos: Position
  }
  final case class Success[+T](value: T, pos: Position)
    extends Result[T]
  final case class Failure(err: String, pos: Position, info: List[(String, Position)] = Nil)
    extends Result[Nothing]
  final case class FatalError(err: String, pos: Position, info: List[(String, Position)])
    extends Result[Nothing]

  class Parser(val input: String) {
    var html = new collection.mutable.StringBuilder()
    var title: Option[String] = None
    var author: Option[String] = None
    var date: Option[String] = None
    var level: Option[String] = None
    var researcher: Option[String] = None
    var boldvenue: Option[String] = None
    var venue: Option[String] = None
    var link: Option[String] = None
    var tags = new collection.mutable.ArrayBuffer[String]()

    var sectionLevel: Int = 1

    final case class Scanner[+T](scan: Position => Result[T]) {
      def mapResult[U](f: Result[T] => Result[U]): Scanner[U] =
        Scanner({ p => f(scan(p)) })

      def mapSuccess[U](f: Success[T] => Success[U]): Scanner[U] =
        mapResult({
          _.map(f)
        })

      def mapValue[U](f: T => U): Scanner[U] =
        mapSuccess({ case Success(v, p) => Success(f(v), p) })

      def >>=[U](f: T => Scanner[U]): Scanner[U] =
        mapResult({
          _ >>= { case Success(v, p) => f(v).scan(p) }
        })

      def commit: Scanner[T] =
        mapResult({
          _.commit
        })

      def ||[U >: T](s: Scanner[U]): Scanner[U] =
        mapResult(_ >>! { f => s.scan(f.pos) })
      /*
        position >>= { p =>
          mapResult({
            _ >>! { _ => s.scan(p) }
          })
        }
        */

      def ~>>[U](s: Scanner[U]): Scanner[T] =
        this >>= { v => s.mapValue({ _ => v }) }

      def >>~[U](s: Scanner[U]): Scanner[U] =
        this >>= { _ => s }

      def withInfo(s: String, pos: Position) =
        mapResult({
          case Failure(e, p, infos) => Failure(e, p, (s, pos) :: infos)
          case FatalError(e, p, infos) => FatalError(e, p, (s, pos) :: infos)
          case s: Success[_] => s
        })
    }

    def nextChar: Scanner[Char] = {
      def scan(p: Position): Result[Char] = {
        val Position(index, line, column) = p
        if (index >= input.length) {
          return Failure("Unexpected end of file", p)
        }

        val c = input.charAt(index)
        val q = if (c == '\n') {
          p.copy(index = index + 1, line = line + 1, column = 0)
        } else {
          p.copy(index = index + 1, column = column + 1)
        }

        Success(c, q)
      }

      Scanner(scan)
    }

    def position: Scanner[Position] =
      Scanner({ p => Success(p, p) })

    def pReturn[T](v: T): Scanner[T] =
      Scanner({ p => Success(v, p) })

    def pFail(s: String): Scanner[Unit] =
      Scanner({ p => Failure(s, p) })

    def backtrack[T](s: Scanner[T]): Scanner[T] =
      position >>= { p =>
        val html_len = html.length
        s.mapResult({
          _ >>! { case Failure(e, _, infos) =>
            html.setLength(html_len)
            Failure(e, p, infos)
          }
        })
      }

    def peek[T](s: Scanner[T]): Scanner[T] =
      position >>= { p =>
        s.mapSuccess({ case Success(v, _) => Success(v, p) })
      }

    def pChar(predicate: Char => Boolean): Scanner[Unit] =
      backtrack(nextChar >>= { c =>
        if (predicate(c)) {
          pReturn(())
        } else {
          pFail("pChar")
        }
      })

    final def pString(s: String): Scanner[Unit] = {
      @tailrec
      def scan(s: String, p: Position): Result[Unit] =
        if (s.isEmpty) {
          Success((), p)
        } else {
          nextChar.scan(p) match {
            case Success(c, q) =>
              if (c == s.charAt(0)) {
                scan(s.substring(1), q)
              } else {
                Failure(s"expected $s", p)
              }
            case f: Failure => f
            case e: FatalError => e
          }
        }

      Scanner({ scan(s, _) })
    }

    def cycle(s: Scanner[Any]): Scanner[Unit] = {
      // (progressing(s) >>= { _ => cycle(s) }) || pReturn(())
      @tailrec
      def scan(p: Position): Result[Unit] =
        progressing(s).scan(p) match {
          case Success(_, q) => scan(q)
          case Failure(_, q, _) => Success((), q)
          case e: FatalError => e
        }

      Scanner(scan)
    }

    def progressing[T](s: Scanner[T]): Scanner[T] =
      position >>= { p =>
        s.mapResult({
          _ >>= { case Success(v, q) =>
            if (q.index > p.index) {
              Success(v, q)
            } else {
              Failure("not progressing", q)
            }
          }
        })
      }

    def until(predicate: Char => Boolean/*, skip: Boolean = false */): Scanner[Unit] =
      cycle(pChar({ !predicate(_) }))

    def skipped(s: Scanner[Any]): Scanner[String] =
      position >>= { p =>
        s.mapSuccess({ case Success(v, q) => Success(input.slice(p.index, q.index), q) })
      }

    def pTitle: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => title = Some(v) })

    def pAuthor: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => author = Some(v.trim) })

    def pDate: Scanner[Unit] = {
      def scan(v: String, p: Position): Result[Unit] = {
        val d = v.trim

        var ok = false
        if (d.length >= 4) {
          val year = d.slice(0, 4)
          ok = year.forall(_.isDigit)
          if (ok && d.length >= 7) {
            val month = d.slice(5, 7)
            ok = d(4) == '-' && month.forall(_.isDigit)
            if (ok && d.length >= 10) {
              val day = d.slice(8, 10)
              ok = d(7) == '-' && day.forall(_.isDigit) && d.length == 10
            }
          }
        }

        if (ok) {
          date = Some(v)
          Success((), p)
        } else {
          Failure("Expected YYYY[-MM[-DD]]", p)
        }
      }

      skipped(until(_ == '}')) >>= { v =>
        Scanner({ scan(v, _) })
      }
    }

    def pResearcher: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => researcher = Some(v.trim) })

    def pLevel: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => level = Some(v.trim) })

    def pVenue: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => venue = Some(v.trim) })

    def pBoldVenue: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => boldvenue = Some(v) })

    def pLink: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ v => link = Some(v) })

    def pTags: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({
        tags ++= _.split(' ')
      })

    def pComment: Scanner[Unit] =
      skipped(until(_ == '}')).mapValue({ _ => () })

    def section: Scanner[Unit] =
      (skipped(until(_ == '\n')) >>= { title =>
        sectionLevel += 1
        html ++= s"<h$sectionLevel>${title.trim}</h$sectionLevel>"
        items
      }).mapValue(_ => {
        sectionLevel -= 1
      })

    def surround[A](htag: String, details: Traversable[(String, String)], s: Scanner[A]): Scanner[A] =
      Scanner({ p =>
        html ++= s"<$htag"
        details.foreach({ case (a, b) => html ++= s""" ${a}="${b}"""" })
        html ++= ">"
        val vp = s.scan(p)
        html ++= s"</$htag>"
        vp
      })

    def emphasize = surround("em", None, content)

    def strong = surround("strong", None, content)

    def deleted = surround("del", None, content)

    def inserted = surround("ins", None, content)

    def superscript = surround("sup", None, content)

    def subscript = surround("sub", None, content)

    def list(htag: String) =
      surround(htag, None,
        cycle(
          until({
            !_.isWhitespace
          }) >>~
            backtrack(surround("li", None, tag(inline = true)))
        )
      )

    def itemize = list("ul")

    def enumerate = list("ol")

    def hyperlink =
      until({
        !_.isWhitespace
      }) >>~
        skipped(until({ _.isWhitespace })) >>=
        { url => surround("a", Array(("href", url)), content) }

    def fontawesome: Scanner[Unit] =
      skipped(until(_ == '}')) mapValue { c =>
        html ++= s"""<i class="$c"></i>"""
      }

    def image: Scanner[Unit] =
      until({ _ == '{' }) >>~ nextChar >>~
        skipped(until({ _ == '}' })) >>= { style =>
          nextChar >>~ until({ !_.isWhitespace }) >>~
          skipped(until({ _.isWhitespace })) >>= { url =>
            skipped(until({ _ == '}' })) mapValue { alt =>
              html ++= s"""<img src="$url" alt="$alt" style="$style"/>""" }}}

    def code(lang: String) =
      surround("pre", None,
        surround("code", Array(("class", s"language-$lang")),
          skipped(until({ !_.isWhitespace }))
            >>= { toSkip =>
            cycle(
              skipped(until({ _ == '\n' }))
                .mapValue({ line => html ++= s"$line\n" })
                >>~ pString(toSkip)
            )
          }
        )
      )

    object CodeTag {
      def apply(lang: String) = s"<$lang>"

      def unapply(s: String): Option[String] = {
        val last = s.length - 1
        if (s.length >= 2 && s.charAt(0) == '<' && s.charAt(last) == '>') {
          Some(s.substring(1, last))
        } else {
          None
        }
      }
    }

    def between[T](open: Scanner[Any], close: Scanner[Any], s: Scanner[T]): Scanner[T] =
      position >>= { p =>
        open >>~
          s ~>>
          position.mapValue(commentPosition(Info, "close", _, input)) ~>>
            close.withInfo("to match opening", p).commit
      }

    // TODO: inline thing not satisfying
    def tag(inline: Boolean): Scanner[Unit] =
      between(pString("{"), pString("}"),
        position >>= { p =>
          skipped(until({ c => "{} \t\n".contains(c) })) >>= { name =>
            commentPosition(Info, name, p, input)
            name match {
              case "" => content
              case "comment" => pComment
              case ":" if !inline => section
              case "." /* if !inline */ => itemize
              case "#" /* if !inline */ => enumerate
              case "!" if inline => emphasize
              case "!!" if inline => strong
              case "-" => deleted
              case "+" => inserted
              case "^" if inline => superscript
              case "_" if inline => subscript
              case "->" if inline => hyperlink
              case "img" => image
              case CodeTag(lang) /* if !inline */ => code(lang)
              case "title" if !inline => pTitle
              case "author" if !inline => pAuthor
              case "date" if !inline => pDate
              case "researcher" if !inline => pResearcher
              case "level" if !inline => pLevel
              case "venue" if !inline => pVenue
              case "!!venue" if !inline => pBoldVenue
              case "link" if !inline => pLink
              case "tags" if !inline => pTags
              case "fa" if inline => fontawesome
              case _ => pFail(s"unknown tag '$name'")
            }
          }
        }
      )

    def paragraph: Scanner[Unit] =
      surround("p", None, // empty paragraphs
        skipped(cycle(
          progressing(skipped(until({ c => "{}\n".contains(c) })))
            .mapValue({ html ++= _ })
            || (pChar({  _ == '\n' }).mapValue(_ => html ++= "\n") >>~ peek(pChar({ _ != '\n' })))
            || tag(inline = true) // TODO: \n\n
        )) >>= { s =>
          if (s.forall(_.isWhitespace)) { pFail("empty paragraph") } else { pReturn(()) }
        }
      )

    def items: Scanner[Unit] =
      cycle(
        until({ !_.isWhitespace }) ~>>
          position.mapValue(commentPosition(Info, "item", _, input)) ~>>
          backtrack(tag(inline = false))
          ||
            position.mapValue(commentPosition(Info, "paragraph", _, input)) ~>>
            backtrack(paragraph) ~>>
              position.mapValue(commentPosition(Info, "paragraph end", _, input))
      )

    def content: Scanner[Unit] =
      cycle(
        progressing(skipped(until({ c => "{}".contains(c) })))
          .mapValue({ html ++= _ })
          || tag(inline = true)
      )
  }
}
