package generator

import collection.generic.Growable

final case class Post(path: Path, title: String, date: String, tags: Traversable[String])

object Post {
  def process(path: Path, posts: Growable[Post]) {
    Markup.parse(path) match {
      case Some(p) => {
        val post = Post(path, p.title.get, p.date.get, p.tags)
        posts += post

        val output = Site.root / "public" / Site.root.relativize(path).withExtension("html")
        Style.genPage(
          output,
          s"""
            |<title>${post.title} - ${Site.author}</title>
            |<link rel="stylesheet" href="/external/prism.css">
            |<script src="/external/prism.js"></script>
          """.stripMargin,
          s"""<h1>${post.title}</h1> <time datetime="${post.date}">${post.date}</time>""",
          p.html.result()
        )
      }
      case None => {}
    }
  }
}
