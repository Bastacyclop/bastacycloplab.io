ThisBuild / scalaVersion := "2.12.7"
ThisBuild / organization := "com.generator"

lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"

lazy val generator = (project in file("."))
  .settings(
    name := "generator",
    libraryDependencies += scalaTest % Test
  )